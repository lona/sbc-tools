#!/bin/bash -e

device=/dev/sdc
image=images/bone-debian-9.9-console-armhf-2019-06-30-1gb.img.xz

block_size=$(sudo blockdev --getbsz ${device})
device_size=$(sudo blockdev --getsize64 ${device})

num_blocks=$(expr ${device_size} / ${block_size})
exit

# make random
dd if=/dev/urandom of=random bs=4096 count=3898368 status=progress
dd if=random of=/dev/sdc bs=4096 count=3898368 status=progress

#md5sum it...
