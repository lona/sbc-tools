#!/bin/bash -e

test "root" == $(whoami) || {
     echo "Please run as superuser"
     exit 1
}

echo -n "SSID: "
read ssid
echo -n "Password: "
read password

read -p "Continue config to connect to ${ssid} with password ${password}? [y/n]" choice

case ${choice} in
     [Yy]* )

     ;;

     * )
     exit 1;;
esac

# Remove static config
rm -f /etc/network/interfaces.d/wlan0

# Write wpa_supplicant file
cat <<EOF > /etc/wpa_supplicant/wpa_supplicant.conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
     ssid="${ssid}"
     psk="${password}"
     key_mgmt=WPA-PSK
}
EOF

# Reconfigure systemctl to not have hostapd
systemctl disable hostapd
systemctl disable dnsmasq
systemctl enable dhcpcd
systemctl enable wpa_supplicant

systemctl reboot


