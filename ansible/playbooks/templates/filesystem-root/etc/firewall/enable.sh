#!/bin/sh
# A very basic IPtables / Netfilter script /etc/firewall/enable.sh

PATH='/sbin'

# Flush the tables to apply changes
iptables -F

iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8000
