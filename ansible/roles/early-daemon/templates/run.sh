#!/bin/bash -e
# This sets up the leds and tries hooking into an led script from the shz location.

DAEMON_SCRIPT={{earlydaemon_script}}

if test -e /sys/class/leds/pca963x:blue; then

    echo "Detected PCA963x LED controller. Fixing permissions."

    for color in red green blue; do
        chmod ugo+rw /sys/class/leds/pca963x:${color}/brightness
    done

    # Make it go dim yellow
    echo 3 > /sys/class/leds/pca963x:red/brightness
    echo 3 > /sys/class/leds/pca963x:green/brightness
    echo 0 > /sys/class/leds/pca963x:blue/brightness

fi

while true; do

    if test ! -e ${DAEMON_SCRIPT}; then
        sleep 1
        continue
    fi

    # Start the daemon script
    ${DAEMON_SCRIPT}

    exit 0
done