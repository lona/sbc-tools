#!/bin/bash
# Save the syslog every 10 minutes to removable storage
set -e

DESTINATION={{savelogs_dest}}
MINUTES=10

mkdir -p  ${DESTINATION}

journalctl --since "${MINUTES} minutes ago" | gzip > ${DESTINATION}/$(date +%s)-incremental-${MINUTES}min.log.gz