# Lona SOC Tools

This is currently a random collection of scripts and toolings for working with SOC boards like Raspberry Pi and BeagleBone

## Ansible Scripts

With `ansible` as current working directory

`./run_playbook.sh playbooks/bootstrap.yml`

## Beaglebone Notes
Images are built upstream
https://github.com/beagleboard/image-builder

